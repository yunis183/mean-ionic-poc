module.exports = function(passport) {
  var User = require('./../model/user'),
    FacebookStrategy = require('passport-facebook').Strategy,
    GoogleStrategy = require('passport-google-openidconnect').Strategy,
    configProviders = {
      facebook: {
        FACEBOOK_APP_ID: "260312223988454",
        FACEBOOK_APP_SECRET: "1225896a815201142cc61b23494e3f7e",
        CALLBACK_URL: "/auth/facebook/callback"
      },
      google: {
        GOOGLE_CLIENT_ID: "679197726118-chjhqn763kvomqvklp192708otrj5ej1.apps.googleusercontent.com",
        GOOGLE_CLIENT_SECRET: "HNqsXQNmZ19xWKYcJswC1rhF",
        CALLBACK_URL: "/auth/google/callback"
      }
    };

  passport.use(User.createStrategy());
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());

  /* Facebook Strategy */
  passport.use(new FacebookStrategy({
    clientID: configProviders.facebook.FACEBOOK_APP_ID,
    clientSecret: configProviders.facebook.FACEBOOK_APP_SECRET,
    callbackURL: configProviders.facebook.CALLBACK_URL
  }, function(accessToken, refreshToken, profile, done) {}));

  /* Google Strategy */
  passport.use(new GoogleStrategy({
    clientID: configProviders.google.GOOGLE_CLIENT_ID,
    clientSecret: configProviders.google.GOOGLE_CLIENT_SECRET,
    callbackURL: configProviders.google.CALLBACK_URL
  }, function(iss, sub, profile, accessToken, refreshToken, done) {
    console.log(profile);
  }));
};
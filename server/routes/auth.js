var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    User = require('./../model/user');

router.get('/login', function(req, res, next){
  res.render('auth/login', { user: req.user, csrf: req.csrfToken() });
});

router.post('/login', passport.authenticate('local'), function(req, res, next){
  req.flash('info', 'Login successfully');
  res.redirect('/');
});

router.post('/register', function(req, res, next){
  console.log(req.body);
  User.register(new User({
    username: req.body.username,
    email: req.body.email,
    admin: true
  }), req.body.password, function(err){
    if (err) {
      console.log('error while user register!', err);
      return next(err);
    }

    console.log('user registered!');

    res.redirect('/');
  });
});

router.get('/facebook', passport.authenticate('facebook'));

router.get('/facebook/callback', passport.authenticate('facebook'), function(req, res, next){
	res.redirect('/');
});

router.get('/google', passport.authenticate('google-openidconnect', {
  scope: ['openid email profile']
}));

router.get('/google/callback', passport.authenticate('google-openidconnect', {
	failureRedirect: '/'
}), function(req, res, next){
	res.redirect('/users');
});

router.get('/logout', function(req, res, next){
	req.logout();
	res.redirect('/');
});

module.exports = router;
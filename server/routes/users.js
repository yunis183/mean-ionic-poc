var express = require('express');
var router = express.Router();
var User = require('./../model/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  User.find({}, function(err, users){
  	res.json(users);
  })
});

router.get('/:username', function(req, res, next){
	User.findOne({"local.username": req.params.username}, function(error, user){
		if (error) res.json(error);
		if (!user) res.status(404).json({ error: "User not found"});
		else res.json(user);
	});
});

router.post('/', function(req, res, next){
	var username = req.body.username;
	User.findOne({ username: username }, function(error, user){
		if (user){
			res.status(409).json({ error: "User already exists" });
		}
		else {
			var user = new User({
				username: req.body.username,
				email: req.body.email,
				firstName: req.body.firstName,
				lastName: req.body.lastName
			});

			user.save(function(error){
				if (error) res.send(error);
				else res.send("Saved");
			});
			res.status(200).json({ user: user });
		}
	})
	
});

module.exports = router;

var express = require('express'),
    router = express.Router(),
    User = require('./../model/user'),
    passport = require('passport'),
    jwt = require('jsonwebtoken');

// find the user
router.post('/authenticate', passport.authenticate('local'), function(req, res){
  var user = req.user;
  
  // if user is found and password is right
  // create a token
  var token = jwt.sign(user, req.app.get('superSecret'), { expiresIn: '1m' });

  // return the information including token as JSON
  res.json({
    success: true,
    message: 'Enjoy your token!',
    token: token
  });
});

// route middleware to verify a token
router.use(function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, req.app.get('superSecret'), function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        console.log(decoded)    
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
});


// Define routers
router.get('/', function(req, res, next){
  res.json({ message: "Hello world" });
});

router.get('/users', function(req, res, next){
  res.json({ users: User.find({}) });
});

module.exports = router;
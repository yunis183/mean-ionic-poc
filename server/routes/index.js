var express = require('express'),
    router = express.Router(),
    User = require('./../model/user'),
    io = require('socket.io')(3001);

router.get('/', function(req, res) {
  res.render('index', { title: 'title'});
});
/*
io.on('connection', function(socket){
  setInterval(function(){
    socket.emit('update', '{"text": "message"}');
  }, 1000);
})
*/
router.get('/profile', isAutheticated, function(req, res){
  res.render('profile');
});

function isAutheticated(req, res, next){
  if (req.user)
    next();
  else
    res.redirect('/login');
}

module.exports = router;

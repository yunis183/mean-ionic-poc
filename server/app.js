/* Packages */
var express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    session = require('express-session'),
    redis = require('redis'),
    redisStore = require('connect-redis')(session),
    i18n = require('i18n'),
    compression = require('compression'),
    flash = require('flash'),
    //io = require('socket.io'),
    //csrf = require('csurf'),
    jwt = require('jsonwebtoken'),

    /* Models */
    User = require('./model/user'),
    
    /* Routes */
    routes = require('./routes/index'),
    users = require('./routes/users'),
    auth = require('./routes/auth'),
    api = require('./routes/api'),

    /* Configs */
    databaseConfig = require('./config/database'),
    passportConfig = require('./config/passport');

var redisClient = redis.createClient();
var app = express();

i18n.configure({
  cookie: 'locale',
  autoReload: true,
  queryParameter: 'lang',
  syncFiles: true,
  defaultLocale: 'es',
  directory: __dirname + '/locales'
});

mongoose.Promise = global.Promise;
mongoose.connect(databaseConfig.url);

passportConfig(passport);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.locals.pretty = true;
app.set('superSecret', 'keyboard cat')

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(session({
  secret: 'keyboard cat',
  store: new redisStore({ host: databaseConfig.redis_url, port: 6379, client: redisClient, ttl :  260}),
  saveUninitialized: false,
  resave: false
}));

app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.init);

app.use(flash());

app.use(compression({
  level: 0
}));

app.use('/', routes);
app.use('/users', users);
app.use('/auth', auth);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.log(err.message);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({ error: err });
});

module.exports = app;
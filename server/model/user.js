var mongoose = require('mongoose'),
		Schema = mongoose.Schema,
		passportLocalMongoose = require('passport-local-mongoose');


var userSchema = mongoose.Schema({
	email: String,
	password: String,
	admin: Boolean,
	username: String,
	facebook: {
		id: String,
		token: String,
		email: String,
		name: String
	},
	twitter: {
		id: String,
		token: String,
		displayName: String,
		username: String
	},
	google: {
		id: String,
		token: String,
		email: String,
		name: String
	}
}, {
	limitAttempts: true
});

userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

userSchema.plugin(passportLocalMongoose, {
	usernameField: "username",
	usernameQueryFields: ["email"],
	usernameUnique: true
});
var User = mongoose.model('User', userSchema);

module.exports = User;
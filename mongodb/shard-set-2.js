var cfg = {
  "_id": "shard-set-2",
  "version": 1,
  "members": [
    {
      "_id": 0,
      "host": "mongo-shard-2-1:27018",
      "priority": 2
    },
    {
      "_id": 1,
      "host": "mongo-shard-2-2:27018",
      "priority": 0
    },
    {
      "_id": 2,
      "host": "mongo-shard-2-3:27018",
      "priority": 0
    }
  ]
};
var error = rs.initiate(cfg);
printjson(error);